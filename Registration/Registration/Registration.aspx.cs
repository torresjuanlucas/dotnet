﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Registration
{
    public partial class Registration : System.Web.UI.Page
    {
        public string GetConnectionString()
        {
            //sets the connection string from your web config file "ConnString" is the name of your Connection String
            return System.Configuration.ConfigurationManager.ConnectionStrings["MyConsString"].ConnectionString;
        }

        private void ExecuteInsert(string firstname, string lastname, string email, string username, string password, string gender, string age)
        {
            SqlConnection conn = new SqlConnection(GetConnectionString());
            string sql = "INSERT INTO Registration (FirstName, LastName, Email, UserName, Password, Gender, Age) VALUES "
                        + " (@FirstName,@LastName,@Email,@UserName,@Password,@Gender,@Age)";

            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlParameter[] param = new SqlParameter[7];
                //param[0] = new SqlParameter("@id", SqlDbType.Int, 20);
                param[0] = new SqlParameter("@FirstName", SqlDbType.VarChar, 50);
                param[1] = new SqlParameter("@LastName", SqlDbType.VarChar, 50);
                param[2] = new SqlParameter("@Email", SqlDbType.VarChar, 50);
                param[3] = new SqlParameter("@UserName", SqlDbType.VarChar, 50);
                param[4] = new SqlParameter("@Password", SqlDbType.VarChar, 50);
                param[5] = new SqlParameter("@Gender", SqlDbType.VarChar, 10);
                param[6] = new SqlParameter("@Age", SqlDbType.Int, 100);
                

                param[0].Value = firstname;
                param[1].Value = lastname;
                param[2].Value = email;
                param[3].Value = username;
                param[4].Value = password;
                param[5].Value = gender;
                param[6].Value = age;
                

                for (int i = 0; i < param.Length; i++)
                {
                    cmd.Parameters.Add(param[i]);
                }

                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "Insert Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }
            finally
            {
                conn.Close();
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TxtPassword.Text == TxtRePassword.Text)
            {
                //call the method to execute insert to the database
                ExecuteInsert(TxtFirstName.Text,
                    TxtLastName.Text,
                    TxtEmail.Text,
                              TxtUserName.Text,
                              TxtPassword.Text,
                              DropDownList1.SelectedItem.Text,
                              TxtAge.Text);
                Response.Write("Record was successfully added!");
                ClearControls(Page);
            }
            else
            {
                Response.Write("Password did not match");
                TxtPassword.Focus();
            }

            


        }
        public static void ClearControls(Control Parent)
        {

            if (Parent is TextBox)
            { (Parent as TextBox).Text = string.Empty; }
            else
            {
                foreach (Control c in Parent.Controls)
                    ClearControls(c);
            }
        }
    }
}